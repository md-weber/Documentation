---
eleventyNavigation:
  key: PushingOutput
  title: Pushing output from SSGs into Codeberg Pages
  parent: CodebergPages
  author: Fayçal Alami Hassani - https://codeberg.org/ka2in
  date: January 2022
  order: 100
---

In case you are using a static site generator (SSG) and that you are satisfied with the result of your project on the local development environment, you can push the files to your Codeberg repository. 

To begin with, we will make sure to have two separate repositories, both locally and on Codeberg:

- A main repository for the source files, i.e. where the source files related to your main project will be located. We will refer to this repository as the `source` repository. This repository is the one associated with your [mydocs](#mydocs) folder. 

- A second repository for Codeberg pages, that we will call the `deployment` repository. This repository will only contain the files available in the `html` folder located under:

```bash
(.venv) $ cd docs/build/html
```

> **Note:**<br />For the purpose of this guide, we have chosen to use two separate folders/repositories. However, you may want to have a different setup that involves creating a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules) instead of having folders and repositories on separate locations. With a submodule, you configuration should look as follows:

```bash
├── sources --> This is a git repo
│   ├── build
│   │   └── html
│   │       └── deployment --> This is a separate git repo
│   └── content
```

To copy the content of the `html` folder to your new `deployment` folder, run the command:

```bash
(.venv) $ cp -R docs/build/html/ deployment 
```

We will now initialize an empty repository inside the deployment folder. To do so, type the command:

```bash
$ git init
```

To check the status of your project’s files, type the command: 

```bash
$ git status
```

To add all the files that are available in your directory to Git, run the command:

```bash
$ git add -A
```

We will then perform a commit with the command:

```bash
$ git commit -m "Example message for your commit"
```

Repeat these steps also in your `source` folder, then push your local commits to the remote repo with the command:

```bash
$ git push origin HEAD:your-remote-branch-name
``` 

> **Note:**<br />Replace `your-remote-branch-name` by the actual name of your remote branch. It is recommended to intially push your commits to a different branch than the `master` or `main` branch. Once you have made sure everything went smoothly, you can then make a pull request to merge contents. To learn more about &ldquo;pull requests&rdquo;, read the article [Pull requests and Git flow](https://docs.codeberg.org/collaborating/pull-requests-and-git-flow/).

To publish the web content of your `deployment` folder on Codeberg, follow the procedure described under the section [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/).

You should now be able to visit your content under the following link: `https://{user-name}.codeberg.page`.