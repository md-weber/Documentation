---
eleventyNavigation:
  key: CodebergPages
  title: Codeberg Pages
  icon: server
  order: 60
---

Codeberg Pages allows you to easily publish static websites with a human-friendly address (`{user-name}.codeberg.page`) via Git on Codeberg.org.
Follow the simple steps below to get started, or check out the advanced usage below.

1. Create a public repository named 'pages' in your user account or organization.
2. Create static content, HTML, style, fonts or images. Name the homepage file `index.html`
3. Push your content to the main branch of the new repository.
4. You should now be able to access your content using the domain `{user-name}.codeberg.page`.

See also [https://codeberg.page/](https://codeberg.page) or the [Troubleshooting](troubleshooting) page.

## Advanced Usage: Canonical URLs

The Codeberg Pages server responds to four different URLs:

- `https://raw.codeberg.page/username/reponame/`: raw content, uses correct MIME types (HTML is forbidden though) and is accessible with CORS.
- `https://username.codeberg.page`: user page, points the default branch of a user's or organization's `pages` repository
- `https://username.codeberg.page/reponame/`: repo page, points to the `pages` branch of the repository
- `https://example.org`: custom domain, points to a repo of choice as outlined below

In all cases, you can append a branch using an `@` (e.g. `https://username.codeberg.page/@develop/README.md`).

## Custom Domains

> **Currently known pitfalls for failed certificates:**  
> - you must either not have a [CAA record](https://en.wikipedia.org/wiki/DNS_Certification_Authority_Authorization#Record), or explicitly allow [LetsEncrypt](https://letsencrypt.org) there

For custom domains, two things are required:
- a `.domains` file in the repository (in the branch in question), containing a list of domains that shall be usable to access that repository:
  -  One domain per line, you can leave lines empty and comment out lines with `#`.
  -  All domains (including `*.codeberg.page`) will be redirected to the first domain in that file.
- a CNAME record pointing to one of the following targets:
  - `username.codeberg.page` → https://username.codeberg.page
  - `reponame.username.codeberg.page` → https://username.codeberg.page/reponame/
  - `branchname.reponame.username.codeberg.page` → https://username.codeberg.page/reponame/@branchname/
  
If you can't use a CNAME record to configure the target (e.g. for a zone root), you can use an A/AAAA/ALIAS record to `codeberg.page` with an additional TXT record for the target (just as shown above for CNAME records).

## Having Questions, Feedback or found a bug?

The source code for Codeberg Pages is maintained over at the [Pages Server repository](https://codeberg.org/Codeberg/pages-server), feel free to head there and providing some feedback, suggestions, bug reports or even patches.
If you need general community support or have questions, [Codeberg/Community](https://codeberg.org/Codeberg/Community) is the better place, as more people will be watching there to help you out!
We really appreciate your contribution.

## Installing Pages for your own Gitea

Codeberg Pages works with any Gitea host out there. So if you are running your own Gitea, you can absolutely run it yourself and help with the development.
Check out the [Pages Server repository](https://codeberg.org/Codeberg/pages-server) for more.