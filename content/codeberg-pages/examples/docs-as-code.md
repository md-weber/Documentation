---
eleventyNavigation:
  key: DocsAsCode
  title: "Example: Docs as Code with Sphinx"
  parent: CodebergPages
  order: 101
---

Docs-as-Code (also known as &ldquo;Documentation as Code&rdquo;) refers to the concept of treating documentation as a codebase, i.e. integrating the documentation process in the toolchains and workflows used by software development teams. 

Docs-as-Code involves implementing versioning capabilities for your documentation projects and authoring your content with plain text markup languages such as Markdown or reStructuredText, among other things. 

To find out more about Docs-as-Code, head over to the official website: [https://docs-as-co.de/](https://docs-as-co.de/). 

Whether you are a single technical communicator or a member of a larger documentation team, you can perfectly apply the Docs-as-Code approach to your documentation projects.

This guide will show you how to implement Docs-as-Code on a Linux machine using [&ldquo;Sphinx&rdquo;](https://www.sphinx-doc.org/en/master/) as a documentation generator, [&ldquo;Git&rdquo;](https://ka2in.codeberg.page/gitinminutes.html) as a version control system and [&ldquo;Codeberg Pages&rdquo;](https://docs.codeberg.org/codeberg-pages/) as a static website hoster.

> **Note**<br />Despite the fact that we are using Sphinx in this particular case, please note that you can easily apply the instructions provided below to any other static site generatior (SSG) such as [Eleventy](https://www.11ty.dev/) or [Hugo](https://gohugo.io/), etc.

## Building documentation with Sphinx

Initially developed to generate Python documentation, Sphinx is now widely used by technical communicators and software developers to create technical and software documentation. 

Sphinx uses [reStructuredText](https://docutils.sourceforge.io/docs/user/rst/quickref.html) as a markup language to generate documentation content. Among further advantages, Sphinx allows you to produce your documentation in different formats, including HTML, LaTeX and ePub. You can also use one of the multiple extensions developed by the community to extend its functionality. 

### Setting up the project

To begin setting up your project, you will first need an account on Codeberg. If you do not already have one, follow the instructions provided under [Your First Steps on Codeberg](https://docs.codeberg.org/getting-started/first-steps/). 

This guide assumes that you already have Git installed on your machine. If this is not the case, please proceed as described in the article [Install Git](https://docs.codeberg.org/getting-started/install-git/).

For the purpose of this guide, we will start a documentation project from scratch using an empty repository. To create a new, empty repository and clone it to your local workspace, follow the steps described in the article [Your First Repository](https://docs.codeberg.org/getting-started/first-repository/) as well as the instructions provided under &rdquo;Option A: Clone the newly created, empty repository&ldquo; within the same article.

### Creating a Python virtual environment

A &rdquo;virtual&ldquo; isolated environment comes in very handy to eliminate the risk of any broken dependencies while working on your projects. Put in other words, setting up a virual environment for your project allows you to run an instance of the Python interpreter with a particular package set and a specific configuration while ensuring compatibility between all these components inside that environment.    

Before proceeding with this task, you should make sure that you have &rdquo;pip&ldquo; and &rdquo;python&ldquo; installed on your machine. To perform a preliminary check, type the following commands in your terminal:

```bash
$ pip --version
$ python --version
```

If pip and Python are not already installed on your Linux machine, please follow the steps described in the articles [pip Installation](https://pip.pypa.io/en/latest/installation/) and [Installing Python 3 on Linux](https://docs.python-guide.org/starting/install3/linux/) respectively.

Depending on your Python version, you will need to install a compatible virtual environment manager, i.e. either &rdquo;venv&ldquo; for Python &gt;&#61; 3.3 , or &rdquo;virtualenv&ldquo; for older Python versions.

> **Note**<br />Please note that Python2 has reached its EOL (End of Life) on January 1st 2020, which means that it is no longer receiving any security updates. If you are using Python 3.3 or higher, you do not need to install the &rdquo;venv&ldquo; module, since it is already available in your Python standard library.

The next step consists in creating a virtual environment. To do so, navigate to the folder where you have cloned the repository described under the section [Setting up the project](#setting-up-the-project):

```bash
$ cd ~/repositories/foobar
$ python3 -m venv env
```

You can replace the second argument &rdquo;env&ldquo; with any other name your like. This will be the folder hosting your virtual environment.

> **Important**<br />
Use a **``.gitignore``** file to exclude your virtual environment directory from your version control system. To ignore the entire &rdquo;env&ldquo; folder, you just have to include the corresponding path and append a **``/``** at its end, e.g. **``env/``**.

### Activating the virtual environment

Before you begin performing any tasks on your project, you must activate your virtual environment by running the following command:

```bash
$ source env/bin/activate
``` 
To make sure that you are working inside your virtual environment, run the command:

```bash
$ which python
```

The output of the command above should be the **`env`** folder, e.g.:

```bash
$ ./env/bin/python
```

Whenever you are done working on your project, run the following command to leave the virtual environment:

```bash
$ deactivate
```

### Installing Sphinx in your virtual environment

If you have followed the steps described up to this point, your local repository should now contain your virtual environment's folder **`env`** in addition to the hidden **`.git`** directory and the **`.gitignore`** file that we have already described at the end of the section [Creating a Python virtual environment](#creating-a-python-virtual-environment), e.g.:

```bash
├── myproject
│   ├── env
│   ├── .git
│   └── .gitignore
```

<a name="mydocs"></a>
You will now create an empty directory then navigate to it. For the purpose of this guide, we will call our directory `mydocs`. Note that you can choose any other name you like:

```bash
$ mkdir mydocs
$ cd mydocs
```

Running the command `tree -a` should now give you the following output: 

```bash
.
├── mydocs
├── env
├── .git
└── .gitignore
```

With your virtual environment activated as shown above, it is now time to install Sphinx with the `pip` tool. To do so, run the command:

```bash
(.venv) $ pip install -U sphinx
```

> **Note**<br /> If you want to find out what the different arguments of **`pip install`** do, type the command **`pip install -h`** in your terminal.

Once Sphinx is installed on your virtual environment, type the following command in your terminal:

```bash
(.venv) $ pip sphinx-quickstart
```

You will then be asked to answer a series of questions regarding the configuration of your project.<br />
Confirm that you want to **`Separate source and build directories`**. For the rest of the questions, keep pressing **Enter** to accept the default values and fill out the requested fields until you reach the question **`autodoc: automatically insert docstrings from modules`**, then choose **&rdquo;y&ldquo; (yes)**. Keep pressing **Enter** again until you reach the question **`Create Makefile?`** and select **&rdquo;y&ldquo; (yes)**.

If you now run the `tree` command from your `mydocs` folder, you should get the following output:

```bash
.
├── build
├── make.bat
├── Makefile
└── source
    ├── conf.py
    ├── index.rst
    ├── _static
    └── _templates
```

Congratulations! 🎉 You have just finished installing Sphinx on your Linux machine.

### Running a local HTTP server using Python

To set up a local testing server for your documentation project, you can use the Python module **`http.server`**. To do so, you will first have to build the html resources for the initial test by running the command:

<a name="build"></a>

```bash
(.venv) $ sphinx-build -b html docs/source/ docs/build/html
``` 

Then navigate to the directory hosting the html resources:

```bash
(.venv) $ cd docs/build/html
```

Once there, run the local python server with the command:

```bash 
(.venv) $ python3 -m http.server 8080
```

> **Note:**<br />&ldquo;8080&rdquo; is the TCP port on which the host server is listening for requests.

To access the default **`index.html`** file, type **`http://localhost:8080`** in your favorite browser. You should then see the default welcome page.

### Creating files with reStructuredText and checking the output

You can start authoring your own content by creating a new reStructuredText file (.rst) in the source directory:

```bash
(.venv) $ cd mydocs/source
(.venv) $ touch mynewfile.rst
```

You have just created an empty &ldquo;.rst&rdquo; file with the **`touch`** command. An &ldquo;.rst&rdquo; file is simply a plain text file that is written with reStructuredText as a markup language. To learn about how to use reStructuredText, follow the instructions provided in the article [reStructuredText Primer](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html). 

You will be more productive in your writing if you use a text editor that supports syntax highlighting such as [Emacs](https://www.gnu.org/software/emacs/download.html) or [Atom](https://atom.io/). Linux offers plenty of powerful text editors that support syntax highlighting and other advanced features for coding and authoring tasks.

> **Note:**<br />Whenever you create a new .rst file, you must add it manually to the `toctree` of your root document, i.e. &ldquo;index.rst&rdquo;. 

The toctree directive (toc stands for Table of Contents) inserts a tree that connects the different &ldquo;.rst&rdquo; files located in your source directory with each other. Read the article [Directives](https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#toctree-directive) to learn more about the toctree directive.

Once you have added an .rst file to the `toctree`, you can check the output at any time by running the [sphinx-build](#running-a-local-http-server-using-python) command illustrated above, then refreshing your local testing server. 

If you are satisfied with the result on your local development environment, you can publish the content on Codeberg Pages. To find out how to achieve this, read the article [Pushing output from SSGs into Codeberg Pages](https://docs.codeberg.org/codeberg-pages/pushing-output/).